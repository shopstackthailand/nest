import { Controller, Get } from '@nestjs/common';
import { MockService } from './mock.service';

@Controller('/mock')
export class MockController {
  constructor(private readonly service: MockService) { }

  @Get()
  list(): Array<any> {
    return this.service.list();
  }
}
