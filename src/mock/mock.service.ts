import { Injectable } from '@nestjs/common';
import mocker from 'mocker-data-generator'

@Injectable()
export class MockService {
  list(pageSize?: number): Array<any> {
    const user = {
      first_name: {
        faker: 'name.firstName'
      },
      last_name: {
        faker: 'name.lastName'
      }
    }
    const res = mocker()
      .schema('users', user, pageSize || 10)
      .buildSync()
    return res?.users
  }
}
